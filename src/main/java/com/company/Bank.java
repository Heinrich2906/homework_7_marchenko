package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by heinr on 10.10.2016.
 */
public class Bank {

    private List<Transaction> transactions = new ArrayList<Transaction>();
    private List<Account> accounts = new ArrayList<Account>();
    private List<Client> clients = new ArrayList<Client>();

//    public void addAccount(Client client) {
//        client.getAccounts().add(new Account(0.0));
//    }

    public boolean addClient(Client client) {

        boolean result;

        result = false;

        if (findClient(client) == null) {

            if (client.getAccounts().size() == 0) client.addAccount(new Account(0.0));
            clients.add(client);
            result = true;
        }

        return result;
    }

    public Client findClient(Client searchClient) {

        if (clients.contains(searchClient)) return searchClient;
        else return null;
    }

    public Account findAccount(long searchAccountNumber) {

        Account result;

        result = null;

        for (Client s : clients) {
            List<Account> temp = s.getAccounts();
            for (Account ss : temp) {
                if (ss.getAccountNumber() == searchAccountNumber) {
                    return ss;
                }
            }
        }

        if (result == null) {
            System.out.print("Счёт с номером " + searchAccountNumber + " не найден.");
        }

        return result;
    }

    public List<Account> findAccountsByClient(Client searchClient) {
        return searchClient.getAccounts();
    }

    public List<Transaction> findTransactionsByAccount(long searchAccountNumber) {

        List<Transaction> myTransaction = new ArrayList<Transaction>();

        for (Transaction s : transactions) {
            if (s.getAmount() == searchAccountNumber) myTransaction.add(s);
        }

        return myTransaction;

    }

    public boolean putIntoAccount(Long accountNumber, Double amount) {

        boolean result;
        result = false;

        if (amount > 0) {

            Account account = findAccount(accountNumber);
            Account myNull = (Account) null;

            if (!account.equals(myNull)) {

                double currentBalance = account.getBalance();
                currentBalance = currentBalance + amount;
                account.setBalance(currentBalance);

                doTransaction(Main.Operation.PUT, account, amount);

                result = true;
            }

        }
        return result;
    }

    public boolean withdrawIntoAccount(Long accountNumber, Double amount) {

        boolean result;
        result = false;

        if (amount > 0) {

            Account account = findAccount(accountNumber);
            Account myNull = (Account) null;

            if (!account.equals(myNull)) {

                double currentBalance = account.getBalance();

                if (currentBalance > amount) {
                    currentBalance = currentBalance - amount;
                    account.setBalance(currentBalance);

                    doTransaction(Main.Operation.WITHDRAW, account, amount);

                    result = true;
                }
            }
        }
        return result;
    }

    public boolean moveFromOneAccountToAnother(Long sourceAccountNumber, Long destinationAccountNumber, Double amount) {

        boolean result;
        result = false;

        if (amount > 0) {

            Account account = findAccount(destinationAccountNumber);
            Account myNull = (Account) null;

            if (!account.equals(myNull)) {

                result = withdrawIntoAccount(sourceAccountNumber, amount);
                if (result == true) {
                    result = putIntoAccount(destinationAccountNumber, amount);
                }
            }
        }
        return result;
    }

    public void printClients() {
        for (Client s : clients) System.out.println(s.toString());
    }

    public void printAccounts() {
        accounts.sort(new Comparator<Account>() {
            @Override
            public int compare(Account o1, Account o2) {
                return Long.compare(o1.getAccountNumber(), o2.getAccountNumber());
            }
        });
        for (Account s : accounts) System.out.println(s.toString());
    }

    public void printAccountsForClient(Client client) {
        for (Account s : client.getAccounts()) System.out.println(s.toString());
    }

    public void doTransaction(Main.Operation operation, Account account, Double amount) {

        Transaction newTransaction = new Transaction(operation, account, amount);
        transactions.add(newTransaction);

    }

    public void printTransactions() {
        for (Transaction s : transactions) System.out.println(s.toString());
    }

    public void printTransactionsForAccount(Long accountNumber) {
        List<Transaction> myTransactions = findTransactionsByAccount(accountNumber);
        for (Transaction s : myTransactions) {
            System.out.println(s.toString());
        }
    }
}



