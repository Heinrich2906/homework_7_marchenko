package com.company;

/**
 * Created by heinr on 10.10.2016.
 */
public class Transaction {

    private Main.Operation operation;   //операция
    private Account source;             //счёт
    private double amount;              //сумма

    Transaction(Main.Operation operation, Account source, double amount) {

        this.operation = operation;
        this.source = source;
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {

        String result;
        result = "Transaction: account number <" + source + ">, operation = <" + operation + ">, amount = <" + amount + ">;";
        return result;
    }

}
