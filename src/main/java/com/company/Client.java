package com.company;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 10.10.2016.
 */
public class Client {

    private String firstName;   // имя
    private String lastName;    // фамилия
    private Date dateOfBirth;   // дата рождения
    private String address;     // адрес
    private String phone;       // телефон
    private List<Account> accounts = new ArrayList<Account>();// список счетов

    Client(String firstName, String lastName, Date dateOfBirth, String address, String phone) {

        this.dateOfBirth = dateOfBirth;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void addAccount(Account account) {
        final boolean add = accounts.add(account);
        if (!add) {
            System.out.println("There is a mistake. Account " + account.toString() + "doesn't add.");
        }
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Client client = (Client) obj;
        return (this.getAddress() == client.getAddress()
                & this.getDateOfBirth() == client.getDateOfBirth()
                & this.getFirstName() == client.getFirstName()
                & this.getLastName() == client.getLastName());

    }

    @Override
    public int hashCode() {

        int result = 1;

        result = 29 * result + getDateOfBirth().hashCode();
        result = 29 * result + getFirstName().hashCode();
        result = 29 * result + getLastName().hashCode();
        result = 29 * result + getAddress().hashCode();

        return result;

    }

    @Override
    public String toString() {

        String result;

        result = "Client " + getFirstName() + ", " + getLastName() +
                ". date of birth " + getDateOfBirth() + ". Mail adress " +
                getAddress() + ", phone number " + getPhone() + " счета " + getAccounts().toString();

        return result;
    }
}
