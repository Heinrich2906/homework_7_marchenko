package com.company;

import java.util.List;

/**
 * Created by heinr on 10.10.2016.
 */
public class Account {

    private Long accountNumber; //  номер счёта
    //private Client client;      //  владелец счёта
    private Double balance;     //  баланс

    public static long GLOBAL_ACCOUNT_NUMBER = 1_000_000L;  //  поле генерации номеров счёта

    Account(Double balance) {

        GLOBAL_ACCOUNT_NUMBER++;

        this.accountNumber = GLOBAL_ACCOUNT_NUMBER;
        this.balance = balance;
        //this.client = client;
    }

    //public Client getClient() {return client;    }

    public Double getBalance() {
        return balance;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    //public void setClient(Client client) { this.client = client;}

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Account account = (Account) obj;
        return (this.getAccountNumber() == account.getAccountNumber()
                //& this.getClient().equals(account.getClient())
                & this.getBalance() == account.getBalance());

    }

    @Override
    public int hashCode() {

        int result = 1;

        result = 29 * result + getAccountNumber().hashCode();
        result = 29 * result + getBalance().hashCode();
        //result = 29 * result + getClient().hashCode();

        return result;
    }

    //увеличивает баланс на заданную сумму
    public void increaseBalance(Double sum) {
        setBalance(getBalance() + sum);
    }

    //уменьшает баланс на заданную сумму
    public void decreaseBalance(Double sum) {
        setBalance(getBalance() - sum);
    }

}
